'use strict';

const express = require("express");
const router = express.Router();
const fs = require("fs");
const path = require("path");

const CONFIG = JSON.parse(process.env.CONFIG);

module.exports = router;

 router.route("/loadPres").get((request, response) => {
    let result = {};

	fs.readdir(CONFIG.presentationDirectory, (err, data) => {
		if (err) {
            response.status(500).send("Error fetch file");
            return;
		}

		data.forEach(file => {
			if (path.extname(file) === ".json") {
                let content  = JSON.parse(fs.readFileSync(CONFIG.presentationDirectory + file, "utf8" ));
                result[content.id] = content;
            }
        });   
        response.status(200).send(result);
    });
});

router.route("/savePres").post((request, response) => {
    fs.writeFile(CONFIG.presentationDirectory + request.body.id + ".pres.json", JSON.stringify(request.body), (err) => {
        if (err) {
            response.status(500).send("Error post file");
            return;
        }
        response.status(200).send(request.body);
    })
});