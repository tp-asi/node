'use strict';

var multer = require("multer");
var express = require("express");
var bodyParser = require("body-parser");
var utils = require("../utils/utils");
var mime = require("mime");
const path = require("path");
var contentController = require('../controllers/content.controller');

const CONFIG = JSON.parse(process.env.CONFIG);

var router = express.Router();

module.exports = router;

// Configuration de multer pour générer des chemins avec extension
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
      callback(null, CONFIG.contentDirectory);
    },
    filename: function (req, file, callback) {
        callback(null, utils.generateUUID() + '.' + mime.extension(file.mimetype));
    }
});
// Image filter pour restreindre l'upload des fichiers sur les images.
var imageFilter = function (req, file, cb) {
    // accept image only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

var multerMiddleware = multer({ storage: storage, fileFilter: imageFilter });

router.route("/contents")
    .get(contentController.list)
    .post(multerMiddleware.single("file"), contentController.create);

router.route("/contents/:contentId")
    .get(contentController.read);

router.param('contentId', function(req, res, next, id) {
    req.contentId = id;
    next();
});