'use strict';

var express = require("express");
var router = express.Router();
module.exports = router;

router.route("/").get(function(request, response){
    response.status(200).send("It works");
})