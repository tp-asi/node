/**
 * Created by benji on 08/11/2017.
 */
// 'use strict';
//
// var express = require("express");
// var router = express.Router();
// var fs = require("fs");
// var path = require("path");
//
// module.exports = router;
//
// router.route("/loadPres").get(function (request, response) {
//
//     let CONFIG = JSON.parse(process.env.CONFIG);
//
//     readdir(CONFIG.presentationDirectory).then(
//         data => readdirSuccess(data, response)
//     ).catch(
//         err => error(err)
//     )
// });
//
// function readdir(dir) {
//     return new Promise(function (resolve, reject) {
//         fs.readdir(dir, function (err, data) {
//             if (err) {
//                 reject(err);
//             }
//             else {
//                 resolve(data)
//             }
//         });
//     });
// }
//
// function readFile(fileName) {
//     return new Promise(function (resolve, reject) {
//         fs.readFile(fileName, function (err, data) {
//             if (err) {
//                 reject(err);
//             }
//             else {
//                 resolve(data)
//             }
//         });
//     });
// }
//
// function readdirSuccess(data, response) {
//     let promises = [];
//     for (let i of data) {
//         let fileName = i;
//         if (path.extname(fileName) === ".json") {
//             promises.push(readFile('./presentation/' + fileName).then(
//                 data => readFileSuccess(data, response)
//             ).catch(
//                 err => error(err)
//             ));
//         }
//     }
//
//     Promise.all(promises).then(
//         data => readAllFileSuccess(data, response)
//     );
// }
//
// function readFileSuccess(data) {
//     return JSON.parse(data);
// }
//
// function readAllFileSuccess(data, response) {
//     response.status(200).send(data);
// }
//
// function error(err) {
//     console.error('ERROR');
//     console.error(err)
// }
//
//
// router.route("/savePres").get(function (request, response) {
//
// });