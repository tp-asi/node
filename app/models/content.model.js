'use strict';
const fs = require("fs");
const utils = require("../utils/utils.js");

module.exports = class Content {
    // méthode constructor
    constructor(content) {
        if (content) {
            this.type = content.type ;
            this.id = content.id;
            this.title = content.title;
            this.src = content.src;
            this.fileName = content.fileName;
        } else {
            this.type = null;
            this.id = null;
            this.title = null;
            this.src = null;
            this.fileName = null;
        }
       
        let data;

        this.getData = function() {
            return data;
        };

        this.setData = function(value) {
            data = value;
        };
    }
  
    static create(content, callback) {
        if (content !== null && content.id && typeof content === 'object') {
            fs.writeFile(utils.getMetaFilePath(content.id), JSON.stringify(content), err => {
                if (err) {
                    console.log("Error create meta file");
                    return callback(err);
                }

                if (content.type === 'img') {
                    // Le fichier peut déjà exister avec l'upload via multer.
                    utils.fileExists(utils.getDataFilePath(content.fileName), err => {
                        // Si pas d'erreur pas besoin de le créer.
                        if (err) {
                            fs.writeFile(utils.getDataFilePath(content.fileName), content.getData(), err => {
                                if (err) {
                                    console.log("Error create file");
                                    return callback(err);
                                }
                                return callback(null);
                            });
                        } else {
                            return callback(null);
                        }                                         
                    });
                } else {

                    return callback(null);
                }
            });
        } else {
            return callback(new Error("Content is not an Object"));
        }
    }

    static read(id, callback) {
        utils.fileExists(utils.getMetaFilePath(id), err => {
            if (err) {
                console.log("file does not exist");
                return callback(err, null)
            }

            fs.readFile(utils.getMetaFilePath(id), "utf8", (err, data) => {
                if (err) {
                    console.log("Error read file");
                    return callback(err, null)
                }
                let content = new Content(JSON.parse(data));

                if(content.type === 'img') {
                    fs.readFile(utils.getDataFilePath(content.fileName), "utf8", (err, data) => {
                        if (err) {
                            console.log("Error read file");
                            return callback(err, null)
                        }
                        content.setData(data);

                        return callback(err, content);
                    })
                } else {
                    return callback(err, content);
                }
            });
        });
    }

    static update(content, callback) {
        if (content !== null && content.id && typeof content === 'object') {
            utils.fileExists(utils.getMetaFilePath(content.id), err => {
                if (err) {
                    console.log("file does not exist");
                    return callback(err)
                }

                fs.writeFile(utils.getMetaFilePath(content.id), JSON.stringify(content), err => {
                    if (err) {
                        console.log("Error create meta file");
                        return callback(err);
                    }

                    if (content.getData() && content.getData().length > 0) {
                        fs.writeFile(utils.getDataFilePath(content.fileName), content.getData(), err => {
                            if (err) {
                                console.log("Error updated file");
                                return callback(err);
                            }
                            return callback(err);
                        });
                    }
                });
            });
        } else {
            return callback(new Error("Content is not an Object"));
        }
    }

    static delete(id, callback) {
        this.read(id, (err, data) => {
            if (err) {
                console.log("Error return file");
                return callback(err);
            }

            fs.unlink(utils.getMetaFilePath(data.id), err => {
                if (err) {
                    console.log("Error remove meta file");
                    return callback(err);
                }

                if (data.type === 'img') {
                    fs.unlink(utils.getDataFilePath(data.fileName), err => {
                        if (err) {
                            console.log("Error remove file");
                            return callback(err);
                        }
                        return callback(err);
                    });
                } else {
                    return callback(err);
                }
            });
        });
    }
};