'use-strict';

var io = require('socket.io');
const fs = require("fs");
var ContentModel = require("../models/content.model");

const CONFIG = JSON.parse(process.env.CONFIG);

let clientMap = new Map();
let currentSlid = 0;
let slidArray = null;
let presentationId = null;

module.exports.listen = function(server) {

    io = io(server);

    io.on('connection', function (socket) {
        socket.emit('connection', 'Vous êtes connecté !');

        socket.on('data_comm', function (clientMessage) {
            console.log(clientMessage);
            clientMap.set(clientMessage.id, socket);
            socket.emit('data_comm', 'OK');
        });

        socket.on('slidEvent', function (data) {
            let sendData = false;
            let loadData = false;
    
            console.log(data.CMD);
            switch(data.CMD) {
                case "START" :
                    presentationId = data.PRES_ID;
                    loadData = true;
                    sendData = true;
                    break;
                case "BEGIN" :
                    currentSlid = 0;
                    sendData = true;
                    break;
                case "PAUSE" :
                    break;
                case "END" :
                    currentSlid = slidArray.length -1;
                    sendData = true;
                    break;
                case "PREV" : 
                    if(currentSlid > 0){
                        currentSlid--;
                    }
                    sendData = true;
                    break;
                case "NEXT" :
                    if(currentSlid < slidArray.length - 1){
                        currentSlid++;
                    }
                    sendData = true;
                    break;
            };
    
            if(presentationId && sendData){
                if(loadData){
                    fs.readFile(`${CONFIG.presentationDirectory}${presentationId}.pres.json`, "utf8", (err, data) => {
                        if(err) {
                            socket.emit("error", "File does not exist");
                            return;
                        }
                        let pres = JSON.parse(data);
                        let promises = [];
    
                        pres.slidArray.forEach(slide => {
                            promises.push(readContent(slide).catch(
                                err => error(err)
                            ));
                        });
    
                        Promise.all(promises).then(
                            slids => {
                                slidArray = slids;
                                sendDataToClients();
                                socket.emit(data.cmd, "OK");
                            }                        
                        );        
                    });
                } else {
                    sendDataToClients();
                    socket.emit(data.cmd, "OK");
                }
            }
        });
    });
}

const readContent = (slide) => {
    return new Promise(function (resolve, reject) {
        ContentModel.read(slide.content_id, function (err, data) {
            if (err) {
                reject(err);
            }
            else {
                let result = {
                    "id": slide.id,
                    "title": slide.title,
                    "txt": slide.txt,
                    "content": data
                }
                resolve(result);
            }
        });
    });
}

const sendDataToClients = () => {
    clientMap.forEach(client => {
        client.emit("slidEvent", slidArray[currentSlid]);
    });
}