'use-strict';

const fs = require("fs");
const path = require("path");
var utils = require("../utils/utils");
var ContentModel = require("../models/content.model");

const CONFIG = JSON.parse(process.env.CONFIG);

module.exports.list = function (request, response) {
    let result = {};

	fs.readdir(CONFIG.contentDirectory, (err, data) => {
		if (err) {
            response.status(500).send("Error fetch file");
            return;
		}

		data.forEach(file => {
			if (path.extname(file) === ".json") {
                let content  = JSON.parse(fs.readFileSync(CONFIG.contentDirectory + file, "utf8" ));
                result[content.id] = content;
            }
        });
        response.status(200).send(result);
    });
}

module.exports.create = function (request, response) {
    let content = request.body;
    // Si le type est image l'id a déjà été généré par multer
    // lors de la création du fichier
    if(content.type === "img"){
        let fileName = request.file.filename;
        if(fileName && fileName.indexOf(".") !== -1){
            content.id = fileName.split('.')[0];
            content.fileName = fileName;
        } else {
            return response.status(400).send("File not found");
        }
    } else {
        content.id = utils.generateUUID();
    }
    ContentModel.create(content, function(err) {
        if (err) {
            response.status(500).send(err);
        }
        response.status(200).send(content);
    });
}

module.exports.read = function (request, response) {
    ContentModel.read(request.contentId, function(err, data) {
        if (err) {
            response.status(500).send(err);
        }
        if(request.query.json){
            response.status(200).send(data);
        }
        else if(data.type === "img"){
            let imgPath = path.resolve(CONFIG.contentDirectory, data.fileName);
            response.status(200).sendFile(imgPath);
        } else {
            response.redirect(data.src);
        }
    });
}