'use strict';

var CONFIG = require("./config.json");
process.env.CONFIG = JSON.stringify(CONFIG);

var express = require("express");
var http = require("http");
var app = express();
var defaultRoute = require("./app/routes/default.route.js");
var presentationRoute = require("./app/routes/presentation.route.js");
var contentRoute = require("./app/routes/content.route.js");
var server = http.createServer(app);
var path = require("path");
var bodyParser = require("body-parser");
var IOController = require("./app/controllers/io.controller.js");

server.listen(CONFIG.port);

IOController.listen(server);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(defaultRoute);
app.use(presentationRoute);
app.use(contentRoute);
app.use("/admin", express.static(path.join(__dirname, "public/admin")));
app.use("/watch", express.static(path.join(__dirname, "public/watch")));